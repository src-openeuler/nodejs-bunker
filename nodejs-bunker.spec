%{?nodejs_find_provides_and_requires}
Name:                nodejs-bunker
Version:             0.1.2
Release:             1
Summary:             Code coverage in native JavaScript
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
License:             MIT
URL:                 https://github.com/substack/node-bunker
Source0:             http://registry.npmjs.org/bunker/-/bunker-%{version}.tgz
BuildRequires:       nodejs-packaging
%description
Bunker is a module to calculate code coverage written in native JavaScript.
It uses AST trickery with the help of the nodejs-burrito package.

%prep
%setup -q -n package

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/bunker
cp -pr package.json index.js %{buildroot}%{nodejs_sitelib}/bunker
%nodejs_symlink_deps

%files
%{nodejs_sitelib}/bunker
%doc README.markdown example

%changelog
* Mon Aug 10 2020 wutao <wutao61@huawei.com> - 0.1.2-1
- package init
